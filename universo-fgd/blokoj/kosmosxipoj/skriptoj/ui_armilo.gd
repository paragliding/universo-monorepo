extends Control


const QueryObject = preload("queries.gd")


var celilo = [] # список целей структурой:
# objekto - указатель на объект-цель
# tasko_uuid - uuid задачи прицеливания
var elektita_celilo = 0 # выбранная цель
var sxipo # указатель на управляемый корабль
var max_celilo = 1 # максимальное количество прицелов
var pafado_id # id запроса на создание проекта ведения огня


var id_pafo = [] # id запроса задачи стрельбы
var pafo_uuid = [] # uuid действующих задач по стрельбе


func _ready():
	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", Callable(self, "_on_data"))
	if err:
		print('error = ',err)


# warning-ignore:unused_argument
func _physics_process(delta):
	# вывод целей на экран
#	if get_node_or_null('Control'):
	# если есть цель или был произведн выстрел и идёт перезарядка, то продолжаем считывать показатели перезарядки
	if (celilo.size()>0) or ($kanonadi/livero.value>0):
		$kanonadi/livero.value = sxipo.armiloj.front().livero
#		else:
#			$Control/celilo.set_visible(false)
#			$Control/celilo/label.text = ''


func _on_data():
	var i_data_server = 0
	var masivo_forigo = [] # массив индексов на удаление
	for on_data in Net.data_server:
		var index_pafo = id_pafo.find(int(on_data['id']))
		if index_pafo > -1: # находится в списке заказа выстрелов, добавляем uuid в список стрельбы
			pafo_uuid.push_back(on_data['payload']['data']['redaktuTaskojTasko']['taskoj']['uuid'])
			id_pafo.remove(index_pafo)
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		elif int(on_data['id']) == pafado_id:
			# пришло сообщение, по созданию проекта стрельбы
			sxipo.pafado_uuid = on_data['payload']['data']['redaktuKreiTaskojProjektoTaskojPosedanto']['projekto']['uuid']
#			send_celilo(sxipo.pafado_celo)
			pafado_id = null
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		i_data_server += 1
	for forigo in masivo_forigo:
		Net.data_server.remove(forigo)


func _on_kanonadi_pressed():
	if celilo.size()>0:
		if $kanonadi.pressed:
#			постановка цели оружию
#			sxipo.get_node("laser_gun").set_target(celilo.front())
			# находим оружие, которым стрелять
			for arm in sxipo.armiloj: # начинаем стрелять всем оружием
				pafo_server(arm, celilo.front()['objekto'])
		else:
			# закрываем задачу ведения огня
#			sxipo.get_node("laser_gun").set_target(null)
			for pafo in pafo_uuid:
				Net.send_json(Queries.finado_tasko(pafo, Net.statuso_fermo))
	else:
		$kanonadi.button_pressed = false
		Title.get_node("CanvasLayer/komunikadoj").aldoni("Цель не выбрана! Определите цель для атаки (огня).")
#		sxipo.get_node("laser_gun").pafado = false


# отправка задачи снятия с прицеливания
func forigo_tasko_celo(celo):
	if !celo.get('uuid'):
		return false
	if celilo.size()<1:
		return false
	# если идёт стрельба по данному объекту, то прекращаем
	if $kanonadi.pressed:
		_on_kanonadi_pressed()
	Net.send_json(Queries.finado_tasko(celilo.front()['tasko_uuid'], Net.statuso_fermo))


# снять с прицеливания
func forigo_celo(celo):
	if !celo.get('uuid'):
		return false
	if celilo.size()<1:
		return false
	# если идёт стрельба по данному объекту, то прекращаем
	if $kanonadi.pressed:
		_on_kanonadi_pressed()
	celilo.clear()
	$celilo.set_visible(false)
	$celilo/label.text = ''
	if len(celilo)<1 and sxipo.pafado_uuid:
		# закрываем проект стрельбы
		Net.send_json(Queries.finado_projekto(sxipo.pafado_uuid))


func add_celo(celo, tasko_uuid):
	celilo.append({
		'objekto': celo,
		'tasko_uuid': tasko_uuid
	})
	$celilo.set_visible(true)
	$celilo/label.text = celo.uuid


# устанавливаем цель
func set_celilo(celo):
	# если количество максимальных прицеливаний больше взятых в прицел, тогда берём в прицел
	if max_celilo<=len(celilo):
		return false
	if !celo.get('uuid'):
		return false
	if sxipo.pafado_uuid: # если проект стрельбы есть, сразу отправляем цель
		send_celilo(celo)
	else:
		# создаём проект ведения огня
		projekto_pafado(celo)


# создание проекта ведения огня вместе с постановкой цели
func projekto_pafado(celo):
	var query = Title.get_node("CanvasLayer/UI/UI/Taskoj").QueryObject.new()
	var taskoj = [] # список задач
	taskoj.append({
		'uuid_celo':celo.uuid,
		'nomo':"прицеливание",
		'priskribo':"Взятие в прицел объекта",
		'kategorio':Net.tasko_kategorio_celilo
	})
	pafado_id = Net.get_current_query_id()
	Net.send_json(query.krei_projekto_taskoj_posedanto(
		"Стрельба",
		"Стрельба по объекту",
		Net.projekto_kategorio_pafado,
		Net.projekto_tipo_objekto,
		Net.statuso_laboranta,
		null,
		0,0,0,0,0,0,
		null,null,
		Net.tipo_posedanto,
		Net.statuso_posedanto,
		taskoj,
		Global.direktebla_objekto[Global.realeco-2]['uuid'],
		Net.tipo_posedanto,
		Net.statuso_posedanto,
		null,
		pafado_id
	))


# отправляем на сервер установку цели
func send_celilo(celo):
	var q = QueryObject.new()
	var id = Net.get_current_query_id()
	Net.net_id_clear.append(id)
	Net.send_json(q.celilo_json(
		sxipo.pafado_uuid,
		Global.direktebla_objekto[Global.realeco-2]['uuid'], # uuid объекта управления
		sxipo.position.x, #kom_koordX
		sxipo.position.y, #kom_koordY
		sxipo.position.z, #kom_koordZ
		celo.uuid,
		celo.position.x, #kom_koordX
		celo.position.y, #kom_koordY
		celo.position.z, #kom_koordZ
		id
	))


# отправляем задачу стрельбы на сервер (передаются объекты в космосе)
# armile - выстреливше оружие
# celo - цель
func pafo_server(armilo, celo):
	Global.saveFileLogs('Выстрел')
	var q = QueryObject.new()
	var id = Net.get_current_query_id()
	id_pafo.append(id)
	Net.send_json(q.pafo_json(
		sxipo.pafado_uuid,
		armilo.uuid, # uuid стреляющего
		sxipo.position.x, #kom_koordX
		sxipo.position.y, #kom_koordY
		sxipo.position.z, #kom_koordZ
		celo.uuid,
		celo.position.x, #kom_koordX
		celo.position.y, #kom_koordY
		celo.position.z, #kom_koordZ
		id
	))


# возвращает взят в прицел или нет данный объект
func get_set_celo(objekto_celo):
	if len(celilo)>0:
		for celo in celilo:
			if celo['objekto'].uuid == objekto_celo['uuid']:
				return false
			else:
				return true
	else:
		return true

