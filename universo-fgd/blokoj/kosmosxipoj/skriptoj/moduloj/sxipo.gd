extends Node
# модель сбора корабля


const base_cabine = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/cabine.tscn")
const base_engine = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/engine.tscn")
const base_cargo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/cargo.tscn")
const canavara_cargo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/VostokU2/Canavara.tscn")
const boozina_cargo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/VostokU2/Boozina.tscn")
const laser = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/laser.tscn")
const laser2 = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/laser2.tscn")
const skif_cabine = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/skif/cabine.tscn")
const skif_engins2 = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/skif/engins2.tscn")
const skif_armilo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/skif/guns.tscn")


# добавление кабины базового корабля
func aldoni_base_cabine(sxipo, objekto=null):
	var cabine = base_cabine.instantiate()
	# x - в право
	# cabine.translation.y = 2 #вверх
	# cabine.translation.z = 1 # назад
	cabine.rotate_y(deg_to_rad(90))
	#cabine.get_node("cabine").translation.y = 2 #вверх
	#cabine.get_node("cabine").translation.z = 2 # назад
	#cabine.get_node("cabine").rotate_y(deg2rad(90))
	#sxipo.get_node("CollisionShape").add_child(cabine)
	if objekto:
		cabine.uuid = objekto['uuid']
		cabine.integreco = objekto['integreco'] 
		cabine.objekto = objekto.duplicate(true)
	else:
		cabine.uuid = '111'
		cabine.integreco = null
		cabine.objekto = null
	sxipo.add_child(cabine)


func aldoni_base_cargo(sxipo, objekto=null):
	# если есть склад, то запрашиваем содержимое склада, если 
	var cargo = base_cargo.instantiate()
	# cargo.translation.y = 2
	cargo.position.z = 6
	cargo.rotate_y(deg_to_rad(90))
	# cargo.get_node("Cargo").translation.y = 2
	# cargo.get_node("Cargo").translation.z = 8
	# cargo.get_node("Cargo").rotate_y(deg2rad(90))
	if objekto:
		cargo.uuid = objekto['uuid']
		cargo.objekto = objekto.duplicate(true)
		cargo.integreco = objekto['integreco']
		# заменяем корпус на корпус Канавары
		if objekto['resurso']['objId'] == 19:
			cargo.get_node('cargo').queue_free()
			cargo.add_child(canavara_cargo.instantiate())
	else:
		cargo.uuid = '111'
		cargo.integreco = null
		cargo.objekto = null
	sxipo.add_child(cargo)


func aldoni_base_engine(sxipo, enganes, objekto=null):
	var engine = base_engine.instantiate()
	if enganes:
		engine.position.x = -2.5
		engine.position.y = -1
		#engine.get_node("engine R").translation.z = 10
		engine.position.z = 9
		engine.rotate_y(deg_to_rad(270))
		engine.rotate_x(deg_to_rad(180))
	else:
		engine.position.x = 2.5
		engine.position.y = -1
		engine.position.z = 9
		engine.rotate_y(deg_to_rad(90))
	if objekto:
		engine.uuid = objekto['uuid']
		engine.integreco = objekto['integreco']
		engine.objekto = objekto.duplicate(true)
	else:
		engine.uuid = '111'
		engine.integreco = null
		engine.objekto = null
	sxipo.add_child(engine)


func aldoni_universala_lasero(sxipo, objekto=null):
	var moduloj = laser.instantiate()
	moduloj.get_node("Turret").position.y = 1.5
	moduloj.get_node("Turret").position.z = -0.9
	moduloj.get_node("Platform").position.y = 1.5
	moduloj.get_node("Platform").position.z = -0.9
	moduloj.get_node("laser").position.y = 2.7
	moduloj.get_node("laser").position.z = -0.9

	moduloj.get_node("gun_body").position.y = 2.2
	moduloj.get_node("gun_body").position.z = -1.4
	if objekto:
		moduloj.uuid = objekto['uuid']
		moduloj.potenco = objekto['stato']['potenco']
		moduloj.integreco = objekto['integreco']
		moduloj.objekto = objekto.duplicate(true)
	else:
		moduloj.uuid = '111'
		moduloj.potenco = null
		moduloj.integreco = null
		moduloj.objekto = null
	#moduloj.get_node("laser/gun_stem").translation.y = 4.2
	moduloj.get_node("laser/gun_stem").position.z = -1.6
	#moduloj.get_node("laser/beam/MeshInstance").translation.y = 4.2
	#moduloj.get_node("laser/beam/MeshInstance2").translation.y = 4.2
	#moduloj.get_node("laser/end_point/Particles").translation.y = 4.2
	moduloj.sxipo = sxipo # установили указатель на корабль управления
	sxipo.add_child(moduloj)
	sxipo.armiloj.append(moduloj) # добавили в массив вооружения корабля


func aldoni_skif_cabine(sxipo, objekto=null):
	var cabine = skif_cabine.instantiate()
	if objekto:
		cabine.uuid = objekto['uuid']
		cabine.integreco = objekto['integreco']
		cabine.objekto = objekto.duplicate(true)
	else:
		cabine.uuid = '111'
		cabine.integreco = null
		cabine.objekto = null
	# x - в право
	cabine.position.y = 2 #вверх
	cabine.position.z = 3 # назад
	cabine.rotate_y(deg_to_rad(180))
	sxipo.add_child(cabine)


func aldoni_skif_cargo(sxipo, objekto=null):
	var cargo = skif_armilo.instantiate()
	cargo.position.y = 2
	cargo.position.z = 3
	cargo.rotate_y(deg_to_rad(180))
	if objekto:
		cargo.uuid = objekto['uuid']
		cargo.integreco = objekto['integreco']
		cargo.objekto = objekto.duplicate(true)
	else:
		cargo.uuid = '111'
		cargo.integreco = null
		cargo.objekto = null
	sxipo.add_child(cargo)


func aldoni_skif_engine(sxipo, objekto=null):
	var engine = skif_engins2.instantiate()
	#engine.translation.x = -2.5
	engine.position.y = 2
	engine.position.z = 3
	engine.rotate_y(deg_to_rad(180))
	if objekto:
		engine.uuid = objekto['uuid']
		engine.integreco = objekto['integreco']
		engine.objekto = objekto.duplicate(true)
	else:
		engine.uuid = '111'
		engine.integreco = null
		engine.objekto = null
	sxipo.add_child(engine)


func aldoni_skif_lasero(sxipo, laser_left, objekto=null):
	var moduloj = laser2.instantiate()
	if laser_left:
		moduloj.get_node("Turret").position.x = -2.5
		moduloj.get_node("Turret").position.y = 2.0
		moduloj.get_node("Turret").position.z = 4.1
		moduloj.get_node("Platform").position.x = -2.5
		moduloj.get_node("Platform").position.y = 2.0
		moduloj.get_node("Platform").position.z = 4.1
		moduloj.get_node("laser").position.x = -2.5
		moduloj.get_node("laser").position.y = 2.0
		moduloj.get_node("laser").position.z = 4.1
		moduloj.get_node("gun_body").position.x = -2.5
		moduloj.get_node("gun_body").position.y = 4.7
		moduloj.get_node("gun_body").position.z = 4.6
		# moduloj.get_node("laser/gun_stem").translation.y = 4.2
		moduloj.get_node("laser/gun_stem").position.z = -0.6
		# moduloj.get_node("laser/beam/MeshInstance").translation.y = 4.2
		# moduloj.get_node("laser/beam/MeshInstance2").translation.y = 4.2
		# moduloj.get_node("laser/end_point/Particles").translation.y = 4.2
		laser_left = !laser_left
	else:
		moduloj.get_node("Turret").position.x = 2.5
		moduloj.get_node("Turret").position.y = 2.0
		moduloj.get_node("Turret").position.z = 4.1
		moduloj.get_node("Platform").position.x = 2.5
		moduloj.get_node("Platform").position.y = 2.0
		moduloj.get_node("Platform").position.z = 4.1
		moduloj.get_node("laser").position.x = 2.5
		moduloj.get_node("laser").position.y = 2.0
		moduloj.get_node("laser").position.z = 4.1
		moduloj.get_node("gun_body").position.x = 2.5
		moduloj.get_node("gun_body").position.y = 4.7
		moduloj.get_node("gun_body").position.z = 4.6
		# moduloj.get_node("laser/gun_stem").translation.y = 4.2
		moduloj.get_node("laser/gun_stem").position.z = -0.6
		# moduloj.get_node("laser/beam/MeshInstance").translation.y = 4.2
		# moduloj.get_node("laser/beam/MeshInstance2").translation.y = 4.2
		# moduloj.get_node("laser/end_point/Particles").translation.y = 4.2
	if objekto:
		moduloj.uuid = objekto['uuid']
		if objekto['stato']:
			moduloj.potenco = objekto['stato']['potenco']
		moduloj.integreco = objekto['integreco']
		moduloj.objekto = objekto.duplicate(true)
	else:
		moduloj.uuid = '111'
		moduloj.potenco = null
		moduloj.integreco = null
		moduloj.objekto = null
	
	moduloj.sxipo = sxipo # установили указатель на корабль управления
	sxipo.add_child(moduloj)
	sxipo.armiloj.append(moduloj) # добавили в массив вооружения корабля


# функция создания корабля
func create_sxipo(sxipo, objecto):
	if !objecto:
		print('Нет такого объекта на сервере')
		return 404
	if (objecto['resurso']['objId'] == 3):# это корабль "Vostok U2" "Базовый космический корабль"
		
		var enganes = false # временно - указывает, что один двигатель уже ставили
		# первый цикл добавляет все модули к кораблю
		var max_konektilo = 0 # максимальный номер комплектующей
		#var tek_konektilo = 1 # текущий номер устанавливаемой в пространстве комплектующей
		# на сколько сдвигать по осям
		#var konektilo_x = 0
		#var konektilo_y = 0
		#var konektilo_z = 0
		# если стандартный тип корабля, то комплектующие не требуются - собираем зная заранее
		# настройки пока нет - всегда стандартный
		if true:
			aldoni_base_cabine(sxipo)
			aldoni_base_cargo(sxipo)
			aldoni_base_engine(sxipo, true)
			aldoni_base_engine(sxipo, false)
			aldoni_universala_lasero(sxipo)
		else:
			if not objecto.get('ligilo'):
				return false
			for modulo in objecto['ligilo']['edges']:
				if max_konektilo < modulo['node']['konektiloPosedanto']:
					max_konektilo = modulo['node']['konektiloPosedanto']
				if modulo['node']['tipo']['objId'] == 1: # связь типа 1 - модуль корабля
					if modulo['node']['ligilo']['resurso']['objId'] == 4: #"Vostok Модуль Кабины" "Базовый модуль кабины кораблей Vostok"
						aldoni_base_cabine(
							sxipo, 
							modulo['node']['ligilo']
						)
					#"Vostok Грузовой Модуль" "Базовый грузовой модуль кораблей Vostok"
					# 	Vostok Canavara Грузовой Модуль
					elif modulo['node']['ligilo']['resurso']['objId'] == 5 or \
							modulo['node']['ligilo']['resurso']['objId'] == 19: 
						aldoni_base_cargo(
							sxipo,
							modulo['node']['ligilo']
						)
					elif modulo['node']['ligilo']['resurso']['objId'] == 6: #"Vostok Двигатель" "Базовый двигатель кораблей Vostok"
						aldoni_base_engine(
							sxipo,
							enganes,
							modulo['node']['ligilo']
						)
						enganes = !enganes
					elif modulo['node']['ligilo']['resurso']['objId'] == 11: #Универсальный лазер
						aldoni_universala_lasero(
							sxipo,
							modulo['node']['ligilo']
						)
	elif (objecto['resurso']['objId'] == 12):# это корабль "Skif EB-1 Poluso" "Базовый малый боевой космический корабль"
		# если стандартный тип корабля, то комплектующие не требуются - собираем зная заранее
		# настройки пока нет - всегда стандартный
		if true:
			aldoni_skif_cabine(sxipo)
			aldoni_skif_cargo(sxipo)
			aldoni_skif_engine(sxipo)
			aldoni_skif_lasero(sxipo, true)
			aldoni_skif_lasero(sxipo, false)
		else:
			# первый цикл добавляет все модули к кораблю
			var max_konektilo = 0 # максимальный номер комплектующей
			for modulo in objecto['ligilo']['edges']:
				if max_konektilo < modulo['node']['konektiloPosedanto']:
					max_konektilo = modulo['node']['konektiloPosedanto']
				if modulo['node']['tipo']['objId'] == 1: # связь типа 1 - модуль корабля
					if modulo['node']['ligilo']['resurso']['objId'] == 13: #"Skif Модуль Кабины"
						aldoni_skif_cabine(sxipo, modulo['node']['ligilo'])
					elif modulo['node']['ligilo']['resurso']['objId'] == 14: #"Skif Боевой Модуль"
						aldoni_skif_cargo(sxipo, modulo['node']['ligilo'])
					elif modulo['node']['ligilo']['resurso']['objId'] == 15: #"Skif Двигательный Модуль"
						aldoni_skif_engine(sxipo, modulo['node']['ligilo'])
					elif modulo['node']['ligilo']['resurso']['objId'] == 16: #Малый Боевой Лазер
						aldoni_skif_lasero(sxipo, true, modulo['node']['ligilo'])
						aldoni_skif_lasero(sxipo, false, modulo['node']['ligilo'])

