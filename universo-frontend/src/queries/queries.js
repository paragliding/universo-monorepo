import gql from 'graphql-tag';

//получить объекты канваса
export const kanvasoObjekto = gql`
  query kanvasojKanvasoObjekto(
    $kanvaso_Uuid: UUID
    $publikigo: Boolean = true
    $forigo: Boolean = false
  ) {
    kanvasojKanvasoObjekto(
      kanvaso_Uuid: $kanvaso_Uuid
      publikigo: $publikigo
      forigo: $forigo
    ) {
      edges {
        node {
          uuid
          forigo
          publikigo
          objId
          pozicio
          koordinatoX
          koordinatoY
          largxo
          longo
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
            objId
          }
          posedanto {
            edges {
              node {
                uuid
              }
            }
          }
          kanvasojKanvasoobjektoligiloLigilo {
            uuid
            tipo {
              objId
              nomo {
                enhavo
              }
            }
            posedanto {
              uuid
              objId
              nomo {
                enhavo
              }
              priskribo {
                enhavo
              }
            }
          }
        }
      }
    }
  }
`;

//Получить канвас со всеми объектами в нём и со связями:
export const kanvasojKanvasoFull = gql`
  query kanvasojKanvasoFull(
    $objId: Float
    $uuid: UUID
    $publikigo: Boolean = true
    $forigo: Boolean = false
  ) {
    kanvasojKanvaso(
      objId: $objId
      uuid: $uuid
      publikigo: $publikigo
      forigo: $forigo
    ) {
      edges {
        node {
          pozicio
          uuid
          forigo
          publikigo
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            id
            objId
            nomo {
              enhavo
            }
            priskribo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                uuid
                objId
                nomo {
                  enhavo
                }
                priskribo {
                  enhavo
                }
              }
            }
          }

          kanvasoObjekto {
            edges {
              node {
                pozicio
                koordinatoX
                koordinatoY
                largxo
                longo
                objId
                uuid
                forigo
                publikigo
                nomo {
                  enhavo
                }
                priskribo {
                  enhavo
                }
                tipo {
                  uuid
                  objId
                  nomo {
                    enhavo
                  }
                  priskribo {
                    enhavo
                  }
                }
                kanvasojKanvasoobjektoligiloLigilo {
                  posedanto {
                    objId
                    uuid
                    nomo {
                      enhavo
                    }
                    priskribo {
                      enhavo
                    }
                    tipo {
                      uuid
                      objId
                      nomo {
                        enhavo
                      }
                      priskribo {
                        enhavo
                      }
                    }
                  }
                  tipo {
                    uuid
                    objId
                    nomo {
                      enhavo
                    }
                    priskribo {
                      enhavo
                    }
                  }
                }
              }
            }
          }
          posedanto {
            edges {
              node {
                tipo {
                  uuid
                  nomo {
                    lingvo
                    enhavo
                    chefaVarianto
                    json
                  }
                  priskribo {
                    lingvo
                    enhavo
                    chefaVarianto
                    json
                  }
                  id
                }
                uuid
                posedantoUzanto {
                  unuaNomo {
                    enhavo
                  }
                  duaNomo {
                    enhavo
                  }
                  uuid
                  sekso
                  avataro {
                    bildoF {
                      url
                    }
                  }
                  familinomo {
                    enhavo
                  }
                  objId
                }
              }
            }
          }
        }
      }
    }
  }
`;

// Получение kanvaso. Если нет uuid - все kanvaso
export const kanvaso = gql`
  query kanvaso(
    $uuid: UUID
    $publikigo: Boolean = true
    $forigo: Boolean = false
  ) {
    kanvasojKanvaso(uuid: $uuid, publikigo: $publikigo, forigo: $forigo) {
      edges {
        node {
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          uuid
          objId
          forigo
          publikigo
        }
      }
    }
  }
`;

export const fetchVersion = gql`
  query fetchVersion {
    universoAplikoVersio(publikigo: true, tipo_Id: 1, aktuala: true) {
      edges {
        node {
          tipo {
            nomo {
              enhavo
            }
          }
          numeroVersio
          numeroSubversio
          numeroKorektado
        }
      }
    }
  }
`;
//запрос для получения списка пользователей
export const uzantojQuery = gql`
  query Uzantoj($first: Int = 1, $after: String, $serchi: String) {
    lasto: uzantoj(last: 1) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    uzantoj(
      first: $first
      after: $after
      isActive: true
      konfirmita: true
      serchi: $serchi
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          isActive
          konfirmita
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`;
//запрос для получения данных пользователя по objID
export const uzantoByObjIdQuery = gql`
  query Uzanto($id: Float!) {
    uzanto: uzantoj(objId: $id) {
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`;
//запрос для получения списка товарищей
export const gekamaradojQuery = gql`
  query Gekamaradoj($first: Int = 1, $after: String, $serchi: String) {
    lasto: gekamaradoj(last: 1) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    gekamaradoj(
      first: $first
      after: $after
      isActive: true
      konfirmita: true
      serchi: $serchi
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          isActive
          konfirmita
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`;
// Запрос для получения списка всех сообществ
export const komunumojQuery = gql`
  query Komunumoj(
    $first: Int = 1
    $after: String
    $enDato: DateTime
    $serchi: String
    $tipo: String
    $forigo: Boolean = false
  ) {
    lasto: komunumoj(last: 1, tipo_Kodo: $tipo) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    komunumoj(
      first: $first
      after: $after
      enDato: $enDato
      serchi: $serchi
      tipo_Kodo_In: $tipo
      orderBy: ["-rating", "-aktiva_dato"]
      forigo: $forigo
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          statistiko {
            postulita
            tuta
            mia
            membraTipo
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          kovrilo {
            bildoE {
              url
            }
          }
        }
      }
    }
  }
`;
// Запрос для получения данных конкретного сообщества по objID
export const komunumoQuery = gql`
  query Komunumo($id: Float) {
    komunumo: komunumoj(objId: $id) {
      edges {
        node {
          id
          objId
          uuid
          tipo {
            kodo
            nomo {
              enhavo
            }
          }
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          statistiko {
            postulita
            tuta
            mia
            membraTipo
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          kovrilo {
            bildoE {
              url
            }
          }
          informo {
            enhavo
          }
          informoBildo {
            bildo
            bildoMaks
          }
          kontaktuloj {
            edges {
              node {
                objId
                unuaNomo {
                  enhavo
                }
                duaNomo {
                  enhavo
                }
                familinomo {
                  enhavo
                }
                avataro {
                  bildoF {
                    url
                  }
                }
                kontaktaInformo
              }
            }
          }
          rajtoj
        }
      }
    }
  }
`;
//запрос для получения списка проектов сообщества по ID сообщества
export const projektojQuery = gql`
  query Projektoj(
    $komunumoId: Float
    $first: Int = 25
    $forigo: Boolean = false
    $arkivo: Boolean = false
    $after: String
  ) {
    projektoj: projektojProjekto(
      first: $first
      projektojprojektoposedanto_PosedantoKomunumo_Id: $komunumoId
      arkivo: $arkivo
      forigo: $forigo
      after: $after
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          id
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          pozicio
          realeco {
            nomo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            nomo {
              enhavo
            }
          }
          objekto {
            nomo {
              enhavo
            }
          }
          projektojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
          tasko {
            edges {
              node {
                id
                #                nomo {
                #                  enhavo
                #                }
                #                priskribo {
                #                  enhavo
                #                }
              }
            }
          }
        }
      }
    }
  }
`;
//запрос для получения данных проекта по UUID. После реализации в бакенде поменяем на ID
export const projektoByUuidQuery = gql`
  query Projekto($uuid: UUID, $objId: Float) {
    projekto: projektojProjekto(uuid: $uuid, objId: $objId) {
      edges {
        node {
          uuid
          id
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          pozicio
          realeco {
            nomo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            nomo {
              enhavo
            }
          }
          objekto {
            nomo {
              enhavo
            }
          }
          projektojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
          tasko {
            edges {
              node {
                uuid
                id
                objId
                nomo {
                  enhavo
                }
                priskribo {
                  enhavo
                }
                kategorio {
                  edges {
                    node {
                      objId
                      nomo {
                        enhavo
                      }
                    }
                  }
                }
                tipo {
                  objId
                  nomo {
                    enhavo
                  }
                }
                statuso {
                  objId
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;
//типы сообществ
export const komunumojTipojQuery = gql`
  query komunumojTipoj {
    komunumojTipoj {
      edges {
        node {
          nomo {
            enhavo
          }
          kodo
          kvantoKomomunumoj
        }
      }
    }
  }
`;
//категории проектов
export const universoProjektojProjektoKategorioQuery = gql`
  query universoProjektojProjektoKategorio {
    kategorio: projektojProjektoKategorio {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//типы проектов
export const universoProjektojProjektoTipoQuery = gql`
  query universoProjektojProjektoTipoQuery {
    tipo: projektojProjektoTipo {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//статусы проектов
export const universoProjektojProjektoStatusoQuery = gql`
  query universoProjektojProjektoStatusoQuery {
    statuso: projektojProjektoStatuso {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//информация о текущем пользователе
export const mi = gql`
  query Mi {
    mi {
      id
      objId
      uuid
      unuaNomo {
        enhavo
      }
      duaNomo {
        enhavo
      }
      familinomo {
        enhavo
      }
      sekso
      konfirmita
      isActive
      chefaLingvo {
        id
        nomo
      }
      chefaTelefonanumero
      chefaRetposhto
      naskighdato
      loghlando {
        nomo {
          lingvo
          enhavo
          chefaVarianto
        }
        id
      }
      loghregiono {
        id
        nomo {
          enhavo
        }
      }
      agordoj
      avataro {
        id
        bildoE {
          url
        }
        bildoF {
          url
        }
      }
      statuso {
        enhavo
      }
      statistiko {
        miaGekamarado
        miaGekamaradoPeto
        kandidatoGekamarado
        tutaGekamaradoj
        rating
        aktivaDato
      }
      kontaktaInformo
      kandidatojTuta
      gekamaradojTuta
      chefaOrganizo {
        id
        nomo {
          enhavo
        }
      }
      sovetoj {
        edges {
          node {
            id
            nomo {
              enhavo
            }
          }
        }
      }
      sindikatoj {
        edges {
          node {
            id
            nomo {
              enhavo
            }
          }
        }
      }
      administritajKomunumoj {
        edges {
          node {
            id
            nomo {
              enhavo
            }
          }
        }
      }
      universoUzanto {
        id
        uuid
        retnomo
      }
      isAdmin
    }
  }
`;
//запрос для получения списка всех проектов
export const tutaProjektojQuery = gql`
  query tutaProjektoj(
    $first: Int = 25
    $after: String
    $forigo: Boolean = false
    $arkivo: Boolean = false
  ) {
    tutaProjektoj: projektojProjekto(
      first: $first
      after: $after
      arkivo: $arkivo
      forigo: $forigo
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          id
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          pozicio
          realeco {
            nomo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            nomo {
              enhavo
            }
          }
          objekto {
            nomo {
              enhavo
            }
          }
          projektojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
          tasko {
            edges {
              node {
                objId
                uuid
                id
                nomo {
                  enhavo
                }
                priskribo {
                  enhavo
                }
              }
            }
          }
        }
      }
    }
  }
`;
//типы задач
export const universoProjektojTaskoTipoQuery = gql`
  query universoProjektojTaskoTipoQuery {
    tipo: projektojTaskoTipo {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//категории задач
export const universoProjektojTaskoKategorioQuery = gql`
  query universoProjektojTaskoKategorioQuery {
    kategorio: projektojTaskoKategorio {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//статусы задач
export const universoProjektojTaskoStatusoQuery = gql`
  query universoProjektojTaskoStatusoQuery {
    statuso: projektojTaskoStatuso {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//запрос на получение данных задачи
export const UniversoProjektojTaskoNode = gql`
  query UniversoProjektojTaskoNode(
    $uuid: UUID
    $arkivo: Boolean = false
    $forigo: Boolean = false
    $objId: Float
  ) {
    projektojTasko(
      uuid: $uuid
      arkivo: $arkivo
      forigo: $forigo
      objId: $objId
    ) {
      edges {
        node {
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          uuid
          kategorio {
            edges {
              node {
                objId
                id
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            objId
            id
            nomo {
              enhavo
            }
          }
          tipo {
            objId
            id
            nomo {
              enhavo
            }
          }
          forigo
          forigaDato
          arkivo
          arkivaDato
        }
      }
    }
  }
`;
//запрос связей проектов
export const UniversoProjektojProjektoLigilo = gql`
  query ligoj(
    $first: Int
    $after: String
    $forigo: Boolean = false
    $arkivo: Boolean = false
    $ligilo_Uuid: UUID
    $posedanto_Uuid: UUID
    $publikigo: Boolean = true
  ) {
    posedanto: projektojProjektoLigilo(
      first: $first
      ligilo_Uuid: $ligilo_Uuid
      after: $after
      arkivo: $arkivo
      forigo: $forigo
      publikigo: $publikigo
    ) {
      edges {
        node {
          uuid
          posedanto {
            nomo {
              enhavo
            }
            priskribo {
              enhavo
            }
            uuid
            objId
          }
          uuid
          tipo {
            nomo {
              enhavo
            }
            uuid
            objId
          }
        }
      }
    }
    ligilo: projektojProjektoLigilo(
      first: $first
      posedanto_Uuid: $posedanto_Uuid
      after: $after
      arkivo: $arkivo
      forigo: $forigo
      publikigo: $publikigo
    ) {
      edges {
        node {
          uuid
          ligilo {
            nomo {
              enhavo
            }
            priskribo {
              enhavo
            }
            uuid
            objId
          }
          uuid
          tipo {
            nomo {
              enhavo
            }
            uuid
            objId
          }
        }
      }
    }
  }
`;
//типы связей проектов
export const ligojTipoj = gql`
  query ligojTipoj {
    projektojProjektoLigiloTipo {
      edges {
        node {
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          objId
        }
      }
    }
  }
`;
//запрос для получения упрощенного списка проектов сообщества по ID сообщества
export const projektojQuerySimple = gql`
  query ProjektojSimple(
    $komunumoId: Float
    $first: Int
    $forigo: Boolean = false
    $arkivo: Boolean = false
    $after: String
  ) {
    projektojSimple: projektojProjekto(
      first: $first
      projektojprojektoposedanto_PosedantoKomunumo_Id: $komunumoId
      arkivo: $arkivo
      forigo: $forigo
      after: $after
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          objId
          nomo {
            enhavo
          }
        }
      }
    }
  }
`;

// Категории документов:

export const dokumentoKategorio = gql`
  query dokumentoKategorio {
    dokumentoKategorio {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
      }
    }
  }
`;

// Типы связей категорий документов между собой
export const DokumentoKategorioLigilojTipoj = gql`
  query DokumentoKategorioLigilojTipoj {
    dokumentoKategorioLigilojTipoj {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
      }
    }
  }
`;

// модель связей категорий документов между собой

export const DokumentoKategorioLigiloj = gql`
  query DokumentoKategorioLigiloj {
    dokumentoKategorioLigiloj {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          posedanto {
            uuid
            nomo {
              enhavo
            }
          }
          ligilo {
            uuid
            nomo {
              enhavo
            }
          }
          tipo {
            objId
            nomo {
              enhavo
            }
          }
        }
      }
    }
  }
`;

// Типы документов в Универсо
export const DokumentoTipo = gql`
  query DokumentoTipo {
    dokumentoTipo {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          realeco {
            edges {
              node {
                objId
                nomo {
                  enhavo
                }
              }
            }
          }
        }
      }
    }
  }
`;

// Виды документов

export const DokumentoSpeco = gql`
  query DokumentoSpeco {
    dokumentoSpeco {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          realeco {
            edges {
              node {
                objId
                nomo {
                  enhavo
                }
              }
            }
          }
        }
      }
    }
  }
`;

// Справочник документов
export const Dokumento = gql`
  query Dokumento(
    $uuid: UUID
    $objId: Float
    $first: Int = 25
    $forigo: Boolean = false
    $arkivo: Boolean = false
    $after: String
  ) {
    dokumento(
      uuid: $uuid
      objId: $objId
      first: $first
      forigo: $forigo
      arkivo: $arkivo
      after: $after
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          realeco {
            edges {
              node {
                objId
                nomo {
                  enhavo
                }
              }
            }
          }
          kategorio {
            edges {
              node {
                objId
                nomo {
                  enhavo
                }
              }
            }
          }
          tipo {
            nomo {
              enhavo
            }
          }
          speco {
            nomo {
              enhavo
            }
          }
        }
      }
    }
  }
`;

// Типы мест хранения документов

export const DokumentoStokejoTipo = gql`
  query DokumentoStokejoTipo {
    dokumentoStokejoTipo {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
      }
    }
  }
`;

// логические места хранения документов
export const DokumentoStokejo = gql`
  query dokumentoStokejo {
    dokumentoStokejo {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          posedanto {
            objId
            nomo {
              enhavo
            }
          }
        }
      }
    }
  }
`;

// Типы владельцев документов

export const DokumentoPosedantoTipo = gql`
  query DokumentoPosedantoTipo {
    dokumentoPosedantoTipo {
      edges {
        node {
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
      }
    }
  }
`;

// Статус владельца документа
export const DokumentoPosedantoStatuso = gql`
  query DokumentoPosedantoStatuso {
    dokumentoPosedantoStatuso {
      edges {
        node {
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
      }
    }
  }
`;

// Владельцы документов

export const DokumentoPosedanto = gql`
  query DokumentoPosedanto {
    dokumentoPosedanto {
      edges {
        node {
          uuid
          realeco {
            objId
            nomo {
              enhavo
            }
          }
          dokumento {
            objId
            nomo {
              enhavo
            }
          }
          tipo {
            objId
            nomo {
              enhavo
            }
          }
          statuso {
            objId
            nomo {
              enhavo
            }
          }
          parto
        }
      }
    }
  }
`;

// Типы связей документов между собой

export const DokumentoLigiloTipo = gql`
  query DokumentoLigiloTipo {
    dokumentoLigiloTipo {
      edges {
        node {
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
      }
    }
  }
`;

// Связь документов между собой

export const DokumentoLigilo = gql`
  query DokumentoLigilo {
    dokumentoLigilo {
      edges {
        node {
          uuid
          priskribo {
            enhavo
          }
          posedanto {
            objId
            nomo {
              enhavo
            }
          }
          ligilo {
            objId
            nomo {
              enhavo
            }
          }
          tipo {
            objId
            nomo {
              enhavo
            }
          }
          konektilo
        }
      }
    }
  }
`;
//Данные по заказам клиента
export const OrdersData = gql`
  query OrdersData(
    $first: Int
    $kliento_Uuid: UUID
    $uuid: UUID
    $orderBy: [String] = ["-kodo"]
    $offset: Int
    $before: String
    $after: String
    $last: Int
    $kodo: String
    $serchi: String
    $marghenoSerchi: String
  ) {
    dokumentoEkspedo(
      kliento_Uuid: $kliento_Uuid
      first: $first
      uuid: $uuid
      orderBy: $orderBy
      offset: $offset
      before: $before
      after: $after
      last: $last
      kodo_Icontains: $kodo
      serchi: $serchi
      marghenoSerchi: $marghenoSerchi
    ) {
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          dokumento {
            projekto {
              edges {
                node {
                  statuso {
                    nomo {
                      enhavo
                    }
                  }
                  tasko {
                    edges {
                      node {
                        statuso {
                          nomo {
                            enhavo
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          nomoKargo {
            enhavo
          }
          uuid
          kodo
          adresoElsxargxado {
            enhavo
          }
          adresoKargado {
            enhavo
          }
          urbojRecivado {
            kodo
            nomo {
              enhavo
            }
          }
          urbojRecivadoNomo {
            enhavo
          }
          urbojTransdono {
            kodo
            nomo {
              enhavo
            }
          }
          urbojTransdonoNomo {
            enhavo
          }
          pezo
          pezoFakto
          pezoKalkula
          volumeno
          kvantoPecoj
          temperaturaReghimo {
            priskribo
            kodo
          }
          kostoKargo
          dangxeraKargo
          klasoDangxera {
            enhavo
          }
          priskriboDangxeraKargo {
            enhavo
          }
          priskriboKargo {
            enhavo
          }
          telefonoAdreso {
            enhavo
          }
          telefonoKliento {
            enhavo
          }
          telefonoAdresoElsxargxado {
            enhavo
          }
          numeroFrajtoletero {
            enhavo
          }
          fordonoKargoDato
          trajnoVenigoDato
          fordonoKargoTempo
          trajnoFinighoDato
          konservejoFordonoDato
          konservejoRicevadoDato
          kreaDato
          ekspedinto {
            nomo {
              enhavo
            }
            priskribo {
              enhavo
            }
          }
          ricevanto {
            uuid
            nomo {
              enhavo
            }
            priskribo {
              enhavo
            }
          }
          rendevuejo
          telefonoKliento {
            enhavo
          }
          volumeno
          pezo
          priskriboKargo {
            enhavo
          }
          kostoKargo
          monsumoKliento
          monsumoTransportisto
          adresoKargado {
            enhavo
          }
          adresoElsxargxado {
            enhavo
          }
          tempoS
          tempoElsxargxado
          konservejoRicevadoDato
          konservejoRicevadoTempoDe
          konservejoRicevadoTempoEn
          konservejoFordonoDato
          konservejoFordonoTempoDe
          konservejoFordonoTempoEn
          organizoAdreso {
            enhavo
          }
          organizoAdresoElsxargxado {
            enhavo
          }
          kombatantoAdresoj {
            enhavo
          }
          kombatantoAdresojElsxargxado {
            enhavo
          }
          urbojRecivadoNomo {
            enhavo
          }
          urbojTransdonoNomo {
            enhavo
          }
          valutoKliento {
            uuid
            id
            objId
            nomo {
              enhavo
            }
          }
          klientoSciigi
          priskriboDangxeraKargo {
            enhavo
          }
          klasoDangxera {
            enhavo
          }
          kombatantoKontrakto {
            uuid
            nomo {
              enhavo
            }
            unuaNomo {
              enhavo
            }
            duaNomo {
              enhavo
            }
            familinomo {
              enhavo
            }
            adreso
          }
          telefonoAdresoElsxargxado {
            enhavo
          }
          telefonoAdreso {
            enhavo
          }
          ekspedoKargo {
            edges {
              node {
                uuid
                nomo {
                  enhavo
                }
                numero
                kvantoPecoj
                longo
                largho
                alto
                pezoFakta
                volumeno
                pezoVolumena
                tipoPakumoj {
                  uuid
                  priskribo
                }
                indikatoro
                numeroIndikatoro {
                  enhavo
                }
                temperaturaReghimo {
                  uuid
                  priskribo
                }
                grave
                retropasxoPakumo
                retropasxoIndikatoro
              }
            }
          }
          dosiero {
            edges {
              node {
                uuid
                id
                nomo {
                  enhavo
                }
                formo {
                  enhavo
                }
                statuso {
                  enhavo
                }
                dosiero
                dosieroUrl
              }
            }
          }
        }
      }
    }
  }
`;
//Количество документов по статусам
// Статусы бывают:
// 7 - В планировании
// 8 - Выполнена
// 9 - На исполнении
// 10 - Отменена
// 11 - Архив
// 12 - Не подтвержденные
// 13 - В пути
// 14 - Доставлена
export const documentsQuantity = gql`
  query documentsQuantity(
    $uuid: UUID
    $statusojId: String = "8 9 10 11 12 13 14"
  ) {
    organizo(uuid: $uuid, statusojId: $statusojId) {
      edges {
        node {
          tutaDokumentoAll
          tutaDokumentoStatusojId
          tutaDokumentoForigi
          tutaDokumentoAktivo
          nomo {
            enhavo
          }
          id
        }
      }
    }
  }
`;

//список предприятий пользователя
export const listOrganizaiton = gql`
  query listOrganizaiton($organizomembro_Uzanto_Id: Int) {
    organizo(organizomembro_Uzanto_Id: $organizomembro_Uzanto_Id) {
      edges {
        node {
          uuid
          nomo {
            enhavo
          }
        }
      }
    }
  }
`;
export const informilojRegionoj = gql`
  query informilojRegionoj($lando_Kodo: String) {
    informilojRegionoj(lando_Kodo: $lando_Kodo) {
      edges {
        node {
          uuid
          kodo
          nomo {
            enhavo
          }
        }
      }
    }
  }
`;
export const informilojLingvoj_informilojLandoj = gql`
  query informilojLingvoj {
    informilojLingvoj {
      edges {
        node {
          uuid
          kodo
          nomo
          flago
        }
      }
    }

    informilojLandoj {
      edges {
        node {
          uuid
          flago
          kodo
          nomo {
            enhavo
          }
          telefonakodo
        }
      }
    }
  }
`;
