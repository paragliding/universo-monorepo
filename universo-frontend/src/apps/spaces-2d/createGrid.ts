import { COLOR_BLACK, COLOR_WHITE } from '../kanbans/Const';
import { EndlessCanvas } from './EndlessCanvas';

export default (scene: EndlessCanvas) => {
  const x = scene.cameras.main.displayWidth / 2;
  const y = scene.cameras.main.displayHeight / 2;
  const width = scene.cameras.main.displayWidth * 10;
  const height = scene.cameras.main.displayHeight * 10;
  const cellWidth = 100;
  const cellHeight = 100;
  const fillColor = COLOR_BLACK;
  const fillAlpha = 0.1;
  const outlineFillColor = COLOR_BLACK;
  const outlineFillAlpha = 0.1;

  const grid = scene.add.grid(
    x,
    y,
    width,
    height,
    cellWidth,
    cellHeight,
    fillColor,
    fillAlpha,
    outlineFillColor,
    outlineFillAlpha,
  );
  return grid;
};
