import { useKanvasoStore } from '../../stores/kanvaso';
import UIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin';
import { Router } from 'vue-router';
import CreateMainMenu from '../main-menu/MainMenu';
import { LoadingResources } from '../resources/LoadingResources';
import CreateToolbar from '../toolbar/CreateToolbar';
import debugUpdater from '../utils/debug/debugUpdater';
import { mainMenuButtons } from '../main-menu/mainMenuButtons';
import { toolbarButtons } from '../toolbar/toolbarButtons';
import zoomButtons from './zoomButtonsAndOther';
import addControls from './controls';
import domElement from './domElement';
import createUiCamera from './createUiCamera';
import createGrid from './createGrid';
import { COLOR_BACKGROUND } from '../kanbans/Const';
import initCanvas from './initCanvas';

export declare const window: {
  router: Router;
} & Window;

export class EndlessCanvas extends Phaser.Scene {
  rexUI: UIPlugin;
  toolbarWidth: number;
  mainMenuHeight: number;
  store: any;
  router: Router;
  isDark: boolean;
  keys: object;
  debug: Phaser.GameObjects.Text[];
  isInputMode: boolean;

  constructor() {
    super({
      key: 'endless-canvas',
    });
    this.store = useKanvasoStore();
    this.router = window.router;

    this.isDark = false;

    this.toolbarWidth = 50;
    this.mainMenuHeight = 48;
    this.isInputMode = false;
  }

  preload() {
    this.keys = this.input.keyboard.addKeys(
      {
        up: 'up',
        down: 'down',
        left: 'left',
        right: 'right',
        space: 'SPACE',
        shift: 'SHIFT',
        ctrl: 'CTRL',
        alt: 'ALT',
      },
      false,
    );

    this.load.scenePlugin(
      'rexuiplugin',
      'rexuiplugin.min.js',
      'rexUI',
      'rexUI',
    );

    LoadingResources(this); // вынес загрузку ресурсов в отдельный файл

    //подключение плагина для сцены анимации загрузки. Временно закомментировал за ненадобностью
    // //@ts-ignore
    // this.load.rexAwait((successCallback, failureCallback) => {
    //   setTimeout(successCallback, 4500);
    // });
    // //Обьявляем закрытие
    // this.plugins
    //   .get('rexLoadingAnimationScene')
    //   //@ts-ignore
    //   .startScene(
    //     this,
    //     'loading-animation',
    //     (successCallback, animationScene) => {
    //       animationScene.onClose(successCallback);
    //     },
    //   );
  }

  create(): void {
    // Скрываем экран загрузки здесь, потому что на этом этапе preload уже завершен
    document.getElementById('loading-screen').style.display = 'none';

    this.cameras.main.setBackgroundColor(COLOR_BACKGROUND);

    createGrid(this);
    initCanvas(this);
    //добавляем элементы управления
    addControls(this);
    // метод для вставки DOM элемента
    // const dom = domElement(this);
    // создаём UI камеру
    createUiCamera(this);

    //toolbar находится здесь потому что он должен создаваться после инициализации UI камеры

    const mainMenu = CreateMainMenu(this, mainMenuButtons(this));
    const toolbar = CreateToolbar(this, toolbarButtons(this));

    // кнопки для зума
    zoomButtons(this);

    this.cameras.main.ignore(mainMenu);
    this.cameras.main.ignore(toolbar);
  }

  update(time: number, delta: number): void {
    // обновляем таблицу с отладочными данными
    debugUpdater(this);
  }
}
