import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';

export default (scene: EndlessCanvas) => {
  scene.input.setDefaultCursor('crosshair');
  let isFirstClick = true; // Флаг для отслеживания первого клика

  // Создаем временный обработчик клика по холсту
  const onCanvasClick = (pointer) => {
    if (isFirstClick) {
      isFirstClick = false; // Сбрасываем флаг при первом клике
      return; // Игнорируем первый клик
    }
    scene.store.onEditKanvasoObjekto({
      koordinatoX: +pointer.worldX.toFixed(0) - 100,
      koordinatoY: +pointer.worldY.toFixed(0) - 100,
      longo: 200,
      largxo: 200,
      nomo: 'Новая доска',
      priskribo: 'Новая доска',
      kanvasoUuid: scene.store.getKanvaso[0].node.uuid,
      tipoId: 1,
    });
    scene.input.setDefaultCursor('default');
    scene.input.off('pointerdown', onCanvasClick); // Удаляем обработчик клика по холсту
  };

  // Добавляем обработчик клика по холсту
  scene.input.on('pointerdown', onCanvasClick);

  // Обработчик нажатия клавиши Esc
  const onEscKey = () => {
    scene.input.setDefaultCursor('default');
    scene.input.keyboard.off('keydown-ESC', onEscKey); // Удаляем обработчик клавиши
    scene.input.off('pointerdown', onCanvasClick); // Удаляем обработчик клика по холсту
  };

  // Добавляем обработчик нажатия клавиши Esc
  scene.input.keyboard.on('keydown-ESC', onEscKey);
};
