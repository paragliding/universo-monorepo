import { COLOR_PRIMARY } from './Const';
import GetMaxTextObjectSize from './GetMaxTextObjectSize';
import CreateTextObject from './CreateTextObject';
import CreatePopupList from './CreatePopupList';
import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
type Options = {
  id: number;
  label: string;
  onClick: () => void;
};
const CreateDropDownList = (
  scene: EndlessCanvas,
  optionsArr?: Options[],
  labelText: string = '▼',
) => {
  const maxTextSize = GetMaxTextObjectSize(
    scene,
    optionsArr.map((o) => o.label),
  );
  const labelMaxTextSize = GetMaxTextObjectSize(scene, [labelText]);
  const label = scene.rexUI.add.label({
    background: scene.rexUI.add
      .roundRectangle(0, 0, 2, 2, 0, COLOR_PRIMARY)
      .setAlpha(0),
    text: CreateTextObject(scene, labelText),
    space: {
      left: 10,
      right: 10,
      top: 10,
      bottom: 10,
      icon: 10,
    },
  });
  // .setData('value', '');

  let menu;

  scene.rexUI.add.click(label).on('click', () => {
    if (menu?.scene) {
      menu.collapse();
      menu = undefined;
      return;
    }
    //@ts-ignore
    const menuX = label.getElement('text').getTopLeft().x;
    const menuY = label.bottom;
    menu = CreatePopupList(
      scene,
      menuX - maxTextSize.width + labelMaxTextSize.width / 2,
      menuY,
      optionsArr,
    );
  });

  return label;
};
export default CreateDropDownList;
