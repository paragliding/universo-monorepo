import ObjectWrapper from 'src/utils/objectWrapper';
import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateDropDownList from './CreateDropDownList';
import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import { EventBus, Events } from 'src/boot/eventBus';
import { createModal } from './CreateModal';

const CreateHeader = (scene: EndlessCanvas, board: ObjectWrapper) => {
  const sizer = scene.rexUI.add
    .sizer({
      orientation: 'x',
      space: {
        left: 5,
        right: 0,
        top: 5,
        bottom: 5,
      },
    })
    .addBackground(scene.rexUI.add.roundRectangle(0, 0, 20, 20, 0, COLOR_DARK));
  const headerLabel = scene.rexUI.add.label({
    text: scene.add.text(0, 0, board.name || ''),
  });

  EventBus.$on(Events.Change, (uuid, object) => {
    if (uuid === board.uuid) {
      //@ts-ignore
      headerLabel.setText(scene.store.getCurrentState.get(uuid).name);
    }
  });
  let textInput = '';

  const dropDownOptions = [
    {
      label: 'Добавить столбец',
      id: 0,
      onClick: () => {
        createModal(scene, (text) => {
          textInput = text;
        }).then((button) => {
          scene.isInputMode = false;

          //@ts-ignore
          if (button.text === 'Сохранить' && textInput) {
            const payload = {
              nomo: textInput,
              //@ts-ignore
              kanvasoUuid: scene.store.getKanvaso[0].node.uuid,
              priskribo: textInput,
              tipoId: 3,
              ligiloPosedantoUuid: board.uuid,
            };

            //@ts-ignore
            scene.store.onEditKanvasoObjekto(payload);
          }
        });
      },
    },
    {
      label: 'Переименовать доску',
      id: 1,
      onClick: () => {
        createModal(
          scene,
          (text) => {
            textInput = text;
          },
          //@ts-ignore
          scene.store.getCurrentState.get(board.uuid).name,
        ).then((button) => {
          scene.isInputMode = false;

          //@ts-ignore
          if (button.text === 'Сохранить' && textInput) {
            const payload = {
              nomo: textInput,
              uuid: board.uuid,
            };
            //@ts-ignore
            scene.store.onEditKanvasoObjekto(payload);
          }
        });
      },
    },
    {
      label: 'Удалить доску',
      id: 2,
      onClick: () => {
        createModal(
          scene,
          null,
          null,
          'Удалить доску',
          'Отменить',
          'Удалить',
        ).then((button) => {
          scene.isInputMode = false;

          //@ts-ignore
          if (button.text === 'Удалить') {
            const payload = {
              uuid: board.uuid,
              forigo: true,
            };
            //@ts-ignore
            scene.store.onEditKanvasoObjekto(payload);
          }
        });
      },
    },
  ];
  const dropDownButton = CreateDropDownList(scene, dropDownOptions);

  sizer
    .add(headerLabel, { proportion: 1, expand: true })
    .add(dropDownButton, { proportion: 0, expand: true });

  return sizer;
};
export default CreateHeader;
